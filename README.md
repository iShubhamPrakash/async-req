# Asynchronous requests Demo

In this project I have made a simple web app which let the user search and get an image along with a list of articles
related to the query.

For image I have used the [Unsplash-API](https://unsplash.com/developers)

For the articles, [New York Times Article search API](https://developer.nytimes.com/) ha been used.

I have used all three methods for the same project to demostrate the way we can make an asynchronous request from an
API.

## Methods used for the async request

> 1) Using XHR method
> 2) Using jQuery's $.ajax() method
> 3) Using the new fetch API

## 1) To Send An Async Request using XHR

* Create an XHR object with the ```XMLHttpRequest``` constructor function
* Use the ```.open()``` method - set the HTTP method and the URL of the resource to be fetched
* Set the ```.onload``` property - set this to a function that will run upon a successful fetch
* Set the ```.onerror``` property - set this to a function that will run when an error occurs
* Use the ```.send()``` method - send the request
* Use the ```.responseText``` property - holds the text of the async request's response

## 2) To Send An Async Request using jQuery

* Add jQuery CDn
* Use the ```$.ajax()``` to send the async request
* Use the ```.done()``` method to handle the success
* No need to parse JSON into js object, it is done automatically

## 3) To send An Async Request using Fetch API

* Use ```fetch()`` method to send async request
* As ```fetch()``` returns a promise, so use ```.then()``` chaining to do further work
* Use ```.then()``` and pass a function to convert received data to JSON using ```response.json()``` method
* Again chain ```.then()``` to handle success
* Use ```.catch()``` method to handle error