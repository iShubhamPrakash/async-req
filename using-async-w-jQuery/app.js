/* eslint-env jquery */

(function () {
    const form = document.querySelector('#search-form');
    const searchField = document.querySelector('#search-keyword');
    let searchedText;
    const responseContainer = document.querySelector('#response-container');

    form.addEventListener('submit', function (e) {
        e.preventDefault();
        responseContainer.innerHTML = '';
        searchedText = searchField.value;

        // For image
        let unsplashURL = "https://api.unsplash.com/search/photos?page=1&query=" + searchedText;
        $.ajax({
            url: unsplashURL,
            headers: {
                Authorization: 'Client-ID d23037952a3b66fb270ca4411258456858ce7aa2d6955d4974cbe5438059a453'
            }
        }).done(generateImage);

        // for articles
        const articleURL = `http://api.nytimes.com/svc/search/v2/articlesearch.json?q=${searchedText}&api-key=cc7d86bda6ea4d11be8dd8173f2d0ede`;
        $.ajax({
            url: articleURL
        }).done(generateArticles);



    });

    function generateImage(data) {
        // console.log("workes");
        // console.log(searchedText);
        // console.log(this.responseText);
        // console.log(data);

        const image = data.results[0];
        let htmlContent = "";
        if (data.results.length) {
            htmlContent = `<figure id="image">
        <img src="${image.urls.regular}" alt="${searchedText}">
        <figcaption>${searchedText} by ${image.user.name}</figcaption>
        </figure>
        `
        } else {
            htmlContent = `<h1 class="error-no-image">Sorry, No image result for  ${searchedText}</h1>`
        }

        responseContainer.insertAdjacentHTML('afterbegin', htmlContent);
    }

    function generateArticles(data) {
        let htmlContent = "";

        // console.log(data.response);

        if (data.response && data.response.docs.length > 1) {
            htmlContent = '<ul>' + data.response.docs.map(article =>
                `<li class="article">
                <h2><a href="${article.web_url}" target="_blank">${article.headline.main}</a></h2>
                <p>${article.snippet}</p>
                </li>`
            ).join('') + '</ul>'
        } else {
            htmlContent = `<h1 class="error-no-article">No Articles for ${searchedText}</h1>`
        }

        // console.log(htmlContent);

        responseContainer.insertAdjacentHTML('beforeend', htmlContent);
    }
})();